import { useEffect, useState } from 'react';

const useFetch = (endpoint) => {
    const [data, setData] = useState([]);
    const baseURL = "http://localhost:9000"
    useEffect(() => {
        fetch(`${baseURL}/${endpoint}`)
            .then(resp => resp.json())
            .then(data => setData(data))
            .catch(console.error)
    }, [baseURL, endpoint])
    return [data, setData];
}

export default useFetch;