import React, { useContext } from 'react';
import AuthContext from '../../context/auh-context';
import DemoErrorBoundary from '../DemoErrorBoundary/DemoErrorBoundary';
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';

const Login = () => {
    const context = useContext(AuthContext);
    const loginClickHandler = () => context.setIsLoggedIn(true)
    return (
        <div>
            <h5>The User is {context.isLoggedIn ? "" : "NOT"} Logged in!</h5>
            <button className='btn btn-warning' 
                onClick={loginClickHandler}>Login</button>
                <hr />
                <ErrorBoundary>
                    <DemoErrorBoundary />
                </ErrorBoundary>
                <ErrorBoundary>
                    <DemoErrorBoundary />
                </ErrorBoundary>
        </div>
    );
}

export default Login;

        // <AuthContext.Consumer>
        //     {
        //         (context) => {
        //             return (
                        
        //             )
        //         }
        //     }
        // </AuthContext.Consumer>