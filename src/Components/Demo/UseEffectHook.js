import React, { useEffect, useState } from 'react';

const UseEffectHook = () => {

    const [toggle, setToggle] = useState(true)
    const [counter, setCounter] = useState(0)

    const [searchTerm, setSearchTerm] = useState('');
    const [repos, setRepos] = useState([]);

    // useEffect(() => {
    //     console.log("Use Effect works");
    //     return () => {
    //         console.log("Clean-up works")
    //     }
    // }, [toggle, counter])

    useEffect(() => {
        let notifier = setTimeout(() => {
            fetch(`https://api.github.com/users/${searchTerm}/repos`)
                .then(resp => resp.json())
                .then(data => setRepos(data))
        }, 1000)
        return () => {
            clearTimeout(notifier)
        }
    }, [searchTerm])

    return (
        <div>
            <h2>Use Effect Demo</h2>

            <input type="text" value={searchTerm}
                onChange={(event) => setSearchTerm(event.target.value)} />
            <hr />
            <ul>
                {repos.length > 0 && repos.map(repo => <li key={repo.id}>{repo.name}</li>)}

            </ul>


            <button className='btn btn-primary'
                onClick={() => setCounter(ctr => ctr + 1)}>{counter}</button>

            <button className='btn btn-dark' onClick={() => setToggle(!toggle)}>Toggle</button>
            {toggle && <p>This paragraph will toggle</p>}
        </div>
    );
}

export default UseEffectHook;
