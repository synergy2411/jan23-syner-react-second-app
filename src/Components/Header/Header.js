import React from 'react';
import { NavLink } from 'react-router-dom'

const Header = () => {
    return (
        <div>
            <nav>
                <ul className='nav nav-tabs'>
                    <li className='nav-item'>
                        <NavLink className="nav-link" to="/">Login</NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink className="nav-link" to="/expenses">Expenses App</NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink className="nav-link" to="/hooks">Hooks</NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink className="nav-link" to="/parent">Parent</NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink className="nav-link" to="/posts">Posts</NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink className="nav-link" to="/counter">Counter</NavLink>
                    </li>
                </ul>
            </nav>
        </div>
    );
}

export default Header;
