import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { increment, decrement, addCounter } from "../../store/counter";

const Counter = () => {
  const dispatch = useDispatch();
  const { value } = useSelector((state) => {
    console.log(state);
    return state.ctr;
  });
  return (
    <div>
      <p className="display-3">Counter : {value}</p>
      <hr />
      <button className="btn btn-primary" onClick={() => dispatch(increment())}>
        Increase
      </button>
      <button
        className="btn btn-secondary"
        onClick={() => dispatch(decrement())}
      >
        Decrease
      </button>
      <button
        className="btn btn-success"
        onClick={() => dispatch(addCounter(10))}
      >
        Add (10)
      </button>
    </div>
  );
};

export default Counter;
