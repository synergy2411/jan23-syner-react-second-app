import React from "react";
import { useState } from "react";
import Button from "../../UI/Button/Button";

const DemoErrorBoundary = () => {
  const [val, setVal] = useState(0);

  if (val > 3) {
    throw new Error("Too high value");
  }
  return (
    <div>
      <Button onClick={() => setVal(val + 1)}>Value : {val}</Button>
      <button
        className="btn btn-danger"
        onClick={() => {
          throw new Error("Error thrown by Event Handler");
        }}
      >
        Throw Error
      </button>
    </div>
  );
};

export default DemoErrorBoundary;
