// import { useContext } from 'react'
// import AuthContext from '../../../context/auh-context'

const ExpenseDate = (props) => {
    const day = props.createdAt.toLocaleString("en-US", { day: "numeric" })
    const month = props.createdAt.toLocaleString("en-US", { month: "long" })
    const year = props.createdAt.getFullYear()

    // const context = useContext(AuthContext);

    return (<p>Created At : {month} {day}, {year}</p>)
    // return (<div>{context.isLoggedIn && <p>Created At : {month} {day}, {year}</p>}</div>)
}

export default ExpenseDate;