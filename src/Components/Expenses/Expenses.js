import { useEffect, useState } from 'react';
import useFetch from '../../hooks/useFetch';
import AddExpense from './AddExpense/AddExpense';
import ExpenseFilter from './ExpenseFilter/ExpenseFilter';
import ExpenseItem from "./ExpenseItem/ExpenseItem";

// const INTIAL_EXPENSES = [{
//     id: "e001",
//     title: "grocery",
//     amount: 12.9,
//     createdAt: new Date("Dec 20, 2022")
// }, {
//     id: "e002",
//     title: "shopping",
//     amount: 19.9,
//     createdAt: new Date("Jan 2, 2021")
// }, {
//     id: "e003",
//     title: "planting",
//     amount: 22.9,
//     createdAt: new Date("Aug 12, 2020")
// }]
let baseURL = "http://localhost:9000";

const Expenses = () => {

    const [show, setShow] = useState(false);
    const [expenses, setExpenses] = useState([]);
    const [selectedYear, setSelectedYear] = useState('2023');
    const [isExpenseDeleted, setIsExpenseDeleted] = useState(false)

    // const [data, setData] = useFetch("expenses")
    // console.log("USE FETCH DATA", data)


    useEffect(() => {
        fetch(`${baseURL}/expenses`)
            .then(resp => resp.json())
            .then(data => {
                let fetchedExpenses = data.map(exp => {
                    return { ...exp, createdAt: new Date(exp.createdAt) }
                })
                setExpenses(fetchedExpenses);
            })
            .catch(console.error)
    }, [isExpenseDeleted])

    const showClickHandler = () => {
        setShow(!show)
    }

    const onAddExpense = (expense) => {
        fetch(`${baseURL}/expenses`, {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify(expense)
        })
            .then(resp => resp.json())
            .then(data => {
                setExpenses((prevExpenses) => [expense, ...prevExpenses]);
            }).catch(console.error)
        setShow(false)
    }

    const onYearSelected = selYear => {
        setSelectedYear(selYear);
    }

    const onDeleteExpense = expId => {
        fetch(`${baseURL}/expenses/${expId}`, {
            method: "DELETE"
        })
            .then(resp => resp.json())
            .then(data => setIsExpenseDeleted(!isExpenseDeleted))
            .catch(console.error)
    }

    let filteredExpenses = [];

    if (expenses.length > 0) {
        filteredExpenses = expenses.filter(exp => exp.createdAt.getFullYear().toString() === selectedYear)

    }

    return (
        <div>
            <div className="row mb-4">
                <div className="col-4">
                    <div className='d-grid'>
                        <button className="btn btn-dark" onClick={showClickHandler}>
                            {show ? 'Hide' : 'Show'} Form </button>
                    </div>
                </div>
                <div className='offset-4 col-4'>
                    <ExpenseFilter selectedYear={selectedYear} onYearSelected={onYearSelected} />
                </div>
                {show && <AddExpense onAddExpense={onAddExpense} showClickHandler={showClickHandler} />}
            </div>
            <br />
            <div className="row">
                {expenses.length > 0 && filteredExpenses.map(exp => (
                    <ExpenseItem expense={exp} key={exp.id} onDeleteExpense={onDeleteExpense}>
                    </ExpenseItem>))}
            </div>
        </div>
    )
}

export default Expenses;