import React, { useState, useRef } from 'react';
import { createPortal } from 'react-dom';

import { v4 } from 'uuid';
import classes from './AddExpense.module.css';

const AddExpense = (props) => {
    const [enteredTitle, setEnteredTitle] = useState('');
    const [enteredAmount, setEnteredAmount] = useState('0.0');
    const [errorMessage, setErrorMessage] = useState('');

    const [isTitleValid, setIsTitleValid] = useState(true);
    const createdAtInputRef = useRef(null);

    const titleChangeHandler = event => setEnteredTitle(event.target.value);
    const amountChangeHandler = event => setEnteredAmount(event.target.value);

    const submitHandler = event => {
        event.preventDefault();
        let newExpense = {
            id: v4(),
            title: enteredTitle,
            amount: Number(enteredAmount),
            createdAt: new Date(createdAtInputRef.current.value)
        }
        props.onAddExpense(newExpense)
    }

    const titleBlurHandler = () => {
        if (enteredTitle === '') {
            setErrorMessage("Title is required")
            return setIsTitleValid(false)
        }
        if (enteredTitle.length < 6) {
            setErrorMessage('Title should have 6 characters');
            return setIsTitleValid(false)
        }
        setErrorMessage('')
    }

    const amountBlurHandler = () => {
    }
    return createPortal((
        <div className={`${classes['backdrop']} row`}>
            <div className='col-8 offset-2'>
                <div className={`${classes['expense-form']} card`}>
                    <div className='card-header'>
                        <h5 className='text-center'>Add New Expense</h5>
                    </div>
                    <div className='card-body'>
                        <form onSubmit={submitHandler}>
                            {/* title */}
                            <div className='form-group mb-2'>
                                <label htmlFor='title'>Title :</label>
                                <input type="text"
                                    name='title'
                                    className='form-control'
                                    value={enteredTitle}
                                    onChange={titleChangeHandler}
                                    onBlur={titleBlurHandler} />
                                {errorMessage !== '' && <div className='alert alert-danger'>
                                    <p>Error : {errorMessage}</p>
                                </div>}
                            </div>
                            {/* amount */}
                            <div className='form-group mb-2'>
                                <label htmlFor='amount'>Amount :</label>
                                <input type="number"
                                    className='form-control'
                                    name='amount'
                                    min="0.0"
                                    step="0.1"
                                    value={enteredAmount}
                                    onChange={amountChangeHandler}
                                    onBlur={amountBlurHandler} />
                            </div>
                            {/* created at */}
                            <div className='form-group mb-3'>
                                <label htmlFor='createdAt'>Created At:</label>
                                <input type="date"
                                    className='form-control'
                                    name="createdAt"
                                    ref={createdAtInputRef} />
                            </div>
                            <div className='form-group'>
                                <div className='d-grid gap-1'>
                                    <button className='btn btn-primary' type='submit'>Add Expense</button>
                                    <button className='btn btn-warning'
                                        onClick={() => props.showClickHandler()}>Close</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    ), document.getElementById("overlay-modal"));
}

export default AddExpense;