import { createSlice } from '@reduxjs/toolkit';

const initialState = { value: 0 }

const counterSlice = createSlice({
    name: "Counter",
    initialState,
    reducers: {
        increment: (state, action) => { state.value += 1 },
        decrement: (state, action) => { state.value -= 1 },
        addCounter: (state, action) => { state.value = state.value + action.payload }
    }
})

export const { increment, decrement, addCounter } = counterSlice.actions;

export default counterSlice.reducer
