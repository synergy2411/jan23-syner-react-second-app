import { configureStore, MiddlewareArray } from '@reduxjs/toolkit';
import counterReducer from './counter';


const logger = state => next => action => {
    console.log("LOGGER STATE : ", state.getState());
    console.log("LOGGER ACTION : ", action);
    return next(action)
}

const store = configureStore({
    reducer: {
        ctr: counterReducer
    },
    devTools: true,
    middleware: new MiddlewareArray().concat(logger)
})

export default store;