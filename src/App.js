import React, { useState, Suspense } from "react";
import { Routes, Route, Navigate } from 'react-router-dom';
import { BounceLoader } from 'react-spinners';
import Counter from "./Components/Counter/Counter";

import Login from "./Components/Demo/Login";
import Parent from "./Components/Demo/Parent";
import UseReducerHook from "./Components/Demo/UseReducerHook";
// import Expenses from "./Components/Expenses/Expenses";
import Header from "./Components/Header/Header";
import PageNotFound from "./Components/PageNotFound/PageNotFound";
import PostItem from "./Components/Posts/PostItem/PostItem";
// import Posts from "./Components/Posts/Posts";
import AuthContext from "./context/auh-context";

const Expenses = React.lazy(() => import("./Components/Expenses/Expenses"))
const Posts = React.lazy(() => import("./Components/Posts/Posts"))

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false)

  return (
    <AuthContext.Provider value={{ isLoggedIn, setIsLoggedIn }}>
      <div className="container">
        <Header />

        <h1>Hello World!!</h1>
        <hr />
        <Suspense fallback={<BounceLoader size={120} color="#ff45ff" />}>
          <Routes>
            {/* http://localhost:3000 */}
            <Route path="/" element={<Navigate to="/login" replace />} />
            <Route path="*" element={<PageNotFound />} />
            <Route path="/login" element={<Login />} />
            <Route path="/expenses" element={<Expenses />} />
            <Route path="/hooks" element={<UseReducerHook />} />
            <Route path="/parent" element={<Parent />} />
            <Route path="/posts/:postId" element={<PostItem />} />
            <Route path="/posts" element={<Posts />} />
            <Route path="/counter" element={<Counter />} />
          </Routes>
        </Suspense>


      </div>
    </AuthContext.Provider>
  );
}

export default App;
